//
//  homeViewController.swift
//  StopWatch
//
//  Created by Reinforce on 03/01/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class homeViewController: UIViewController {
    
    @IBOutlet weak var lbltime: UILabel!
    
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnLap: UIButton!
    @IBOutlet weak var btnPLAyPause: UIButton!
    
    //Lap view
    @IBOutlet weak var viewLap : UIView!
    @IBOutlet weak var tblLap : UITableView!
    
    @IBOutlet weak var bottomViewLap : NSLayoutConstraint!
    
    
    @IBOutlet weak var viewMainCircular : UIView!
  //  @IBOutlet weak var viewDashCircular : UIView!
    
    var timer = Timer()
    //var counter = 0.0
    var isRunning = false
    
    var addLap = false
    
    var min = 0
    var sec = 0
    var fractions = 0
    var strstopWatch = ""
    
    var arrLapTime : [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.btnLap.isHidden = true
        self.btnReset.isHidden = true
        
        lbltime.text = "00:00.00"
        
        self.setView(view: self.viewLap, hidden: true)
        UIView.animate(withDuration: 1, animations: {
            self.bottomViewLap.constant = 264
        }, completion: nil)
        
       // overrideUserInterfaceStyle = .dark
        
        
        let gradient = CAGradientLayer()
        
        gradient.frame = viewMainCircular.bounds
        gradient.colors = [UIColor.black.cgColor, UIColor.init(red: 111/255, green: 111/255, blue: 111/255, alpha: 1.0).cgColor]

        gradient.startPoint = CGPoint.zero
        gradient.endPoint = CGPoint(x: 1, y: 1)

        viewMainCircular.layer.insertSublayer(gradient, at: 0)
        
        
        
//
//        let path = UIBezierPath(roundedRect: rect, cornerRadius: 0)
//        UIColor.clear.setFill()
//        path.fill()
//
//        .setStroke()
//        path.lineWidth = 5
//
//        let dashPattern : [CGFloat] = [10, 4]
//        path.setLineDash(dashPattern, count: 2, phase: 0)
//        path.stroke()
//
//        var yourViewBorder = CAShapeLayer()
//        yourViewBorder.strokeColor = UIColor.init(red: 111/255, green: 111/255, blue: 111/255, alpha: 1.0).cgColor
//        yourViewBorder.lineDashPattern = [10, 4]
//        yourViewBorder.frame = viewDashCircular.bounds
//        yourViewBorder.fillColor = nil
//        yourViewBorder.path = UIBezierPath(rect: viewDashCircular.bounds).cgPath
//        viewDashCircular.layer.addSublayer(yourViewBorder)


    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
      }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickONPlayBtn(_ sender: UIButton)
    {
        if sender.isSelected == true {
          sender.isSelected = false
            
            //self.btnLap.isHidden = true
            //self.btnReset.isHidden = true
            timer.invalidate()
            isRunning = false
            addLap = true
            
        }else {
          sender.isSelected = true
            
            self.btnLap.isHidden = false
            self.btnReset.isHidden = false
            
            if !isRunning{
                timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(UpdateTime), userInfo: nil, repeats: true)
             isRunning = true
              addLap = true
                
            }
        }
    }
    
    
    
    @IBAction func clickOnBtnReset(_ sender: UIButton) {
        timer.invalidate()
        
        addLap = false
        fractions = 0
        min = 0
        sec = 0
        
        strstopWatch = "00:00.00"
        lbltime.text = strstopWatch
        
        self.setView(view: self.viewLap, hidden: true)
        self.arrLapTime = []
        self.tblLap.reloadData()
        
        self.btnLap.isHidden = true
        self.btnReset.isHidden = true
        btnPLAyPause.isSelected = false
        isRunning = false
        addLap = true
    }
    
    @objc func UpdateTime(){
       // counter += 0.1
        //= String(format: "%.1f", counter)
        
        fractions += 1
        if fractions == 100{
            
            sec += 1
            fractions = 0
        }
        
        if sec == 60{
            
            min += 1
            sec = 0
        }
        
        let strFrac = fractions > 9 ? "\(fractions)" : "0\(fractions)"
        let strSec = sec > 9 ? "\(sec)" : "0\(sec)"
        let strMin = min > 9 ? "\(min)" : "0\(min)"
        
        strstopWatch = "\(strMin):\(strSec).\(strFrac)"
        lbltime.text  = strstopWatch
        
    }

    
    @IBAction func clickOnLapBtn(_ sender: UIButton)
    {
        self.setView(view: self.viewLap, hidden: false)
        UIView.animate(withDuration: 1, animations: {
                   self.bottomViewLap.constant = 0
               }, completion: nil)
        arrLapTime.insert(strstopWatch, at: 0)
        tblLap.reloadData()
    }
}

extension homeViewController : UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLapTime.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblLap.dequeueReusableCell(withIdentifier: "tblLapCell") as! tblLapCell
        cell.lblIndex.text = "\u{2022} \(indexPath.row)"
        cell.lblLapTime.text = arrLapTime[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
}



class tblLapCell : UITableViewCell
{
    @IBOutlet weak var lblLapTime : UILabel!
    @IBOutlet weak var lblIndex : UILabel!
}


